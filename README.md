# FLOSS Marche


Work in progress for the new website of the FLOSS Marche community!

This site was made using Hugo (see info http://gohugo.io).

# Building web site in local

First, you need to install Hugo.
http://gohugo.io/overview/installing/
After you have it in your system you can just write:
hugo server
Now, your floss server is in http://localhost:1313/
That all folks! ;)

#Web site description

This web site is composed of:
- Index page: where there is a small description of the association,
- Lug page: A table where all lug are listed with name, logo, web site link and link to the mailing list and a map with all lugs (see lugmap),
- Meeting between Lug page: A list of all events with the possibility to add to the event,
- Event page: An iframe to lugmap.
- Mailing list: A form with several informations like name,surname and email that allows you to enter in the floss mailing list.
